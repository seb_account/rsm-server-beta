/*
  h2-console
default db name:jdbc:h2:mem:testdb
 */
--roles
INSERT INTO ROLE(ROLE_ID,ROLE_NAME,ROLE_DESCRIPTION) VALUES(1,'ROLE_ADMIN','Admin Role');
INSERT INTO ROLE(ROLE_ID,ROLE_NAME,ROLE_DESCRIPTION) VALUES(2,'ROLE_EMPLOYEE','Employee Role');
INSERT INTO ROLE(ROLE_ID,ROLE_NAME,ROLE_DESCRIPTION) VALUES(3,'ROLE_CUSTOMER','Customer Role');

--accounts
/*password=password */
--employee_accounts
INSERT INTO ACCOUNT(USERNAME,PASSWORD,ENABLED,CREDENTIALS_EXPIRED,EXPIRED,LOCKED)
VALUES ('adam_adamowicz','$2a$10$aiBgroa5UbCph3gb4WSiD.njeKqgsjXmHv/pbXiRVAYPMlewWDLD6',true,false,false,false);
INSERT INTO ACCOUNT(USERNAME,PASSWORD,ENABLED,CREDENTIALS_EXPIRED,EXPIRED,LOCKED)
VALUES ('jan_kowalski','$2a$10$aiBgroa5UbCph3gb4WSiD.njeKqgsjXmHv/pbXiRVAYPMlewWDLD6',true,false,false,false);
INSERT INTO ACCOUNT(USERNAME,PASSWORD,ENABLED,CREDENTIALS_EXPIRED,EXPIRED,LOCKED)
VALUES ('dariusz_nowak','$2a$10$aiBgroa5UbCph3gb4WSiD.njeKqgsjXmHv/pbXiRVAYPMlewWDLD6',true,false,false,false);

--customer_accounts
INSERT INTO ACCOUNT(USERNAME,PASSWORD,ENABLED,CREDENTIALS_EXPIRED,EXPIRED,LOCKED)
VALUES ('adam_klient','$2a$10$aiBgroa5UbCph3gb4WSiD.njeKqgsjXmHv/pbXiRVAYPMlewWDLD6',true,false,false,false);
INSERT INTO ACCOUNT(USERNAME,PASSWORD,ENABLED,CREDENTIALS_EXPIRED,EXPIRED,LOCKED)
VALUES ('bartosz_klient','$2a$10$aiBgroa5UbCph3gb4WSiD.njeKqgsjXmHv/pbXiRVAYPMlewWDLD6',true,false,false,false);
INSERT INTO ACCOUNT(USERNAME,PASSWORD,ENABLED,CREDENTIALS_EXPIRED,EXPIRED,LOCKED)
VALUES ('cezary_klient','$2a$10$aiBgroa5UbCph3gb4WSiD.njeKqgsjXmHv/pbXiRVAYPMlewWDLD6',true,false,false,false);

--account_role
INSERT INTO ACCOUNT_ROLE(ACCOUNT_ID,ROLE_ID) SELECT a.ACCOUNT_ID,r.ROLE_ID FROM ACCOUNT a,ROLE r WHERE a.USERNAME='adam_adamowicz'
and r.ROLE_ID=1;
INSERT INTO ACCOUNT_ROLE(ACCOUNT_ID,ROLE_ID) SELECT a.ACCOUNT_ID,r.ROLE_ID FROM ACCOUNT a,ROLE r WHERE a.USERNAME='jan_kowalski'
and r.ROLE_ID=2;
INSERT INTO ACCOUNT_ROLE(ACCOUNT_ID,ROLE_ID) SELECT a.ACCOUNT_ID,r.ROLE_ID FROM ACCOUNT a,ROLE r WHERE a.USERNAME='dariusz_nowak'
and r.ROLE_ID=2;

INSERT INTO ACCOUNT_ROLE(ACCOUNT_ID,ROLE_ID) SELECT a.ACCOUNT_ID,r.ROLE_ID FROM ACCOUNT a,ROLE r WHERE a.USERNAME='adam_klient'
and r.ROLE_ID=3;
INSERT INTO ACCOUNT_ROLE(ACCOUNT_ID,ROLE_ID) SELECT a.ACCOUNT_ID,r.ROLE_ID FROM ACCOUNT a,ROLE r WHERE a.USERNAME='bartosz_klient'
and r.ROLE_ID=3;
INSERT INTO ACCOUNT_ROLE(ACCOUNT_ID,ROLE_ID) SELECT a.ACCOUNT_ID,r.ROLE_ID FROM ACCOUNT a,ROLE r WHERE a.USERNAME='cezary_klient'
and r.ROLE_ID=3;


--address
--employee_addresses
INSERT INTO ADDRESS(ADDRESS_ID,STREET_NAME,APARTMENT_NUMBER,CITY,ZIP_CODE,STATE_VOIVODESHIP)
VALUES (1,'ul.Polna 123','13','Warszawa','00-001','Mazowieckie');
INSERT INTO ADDRESS(ADDRESS_ID,STREET_NAME,APARTMENT_NUMBER,CITY,ZIP_CODE,STATE_VOIVODESHIP)
VALUES (2,'ul.Marynarska 22','15','Warszawa','00-003','Mazowieckie');
INSERT INTO ADDRESS(ADDRESS_ID,STREET_NAME,APARTMENT_NUMBER,CITY,ZIP_CODE,STATE_VOIVODESHIP)
VALUES (3,'ul.Hiacyntowa 45','27','Warszawa','00-005','Mazowieckie');


--customer_addresses
INSERT INTO ADDRESS(ADDRESS_ID,STREET_NAME,APARTMENT_NUMBER,CITY,ZIP_CODE,STATE_VOIVODESHIP)
VALUES (4,'ul.Stefanowskiego 17','55','Lodz','92-222','Lodzkie');
INSERT INTO ADDRESS(ADDRESS_ID,STREET_NAME,APARTMENT_NUMBER,CITY,ZIP_CODE,STATE_VOIVODESHIP)
VALUES (5,'ul.Piotrkowska 111','36','Lodz','92-444','Lodzkie');
INSERT INTO ADDRESS(ADDRESS_ID,STREET_NAME,APARTMENT_NUMBER,CITY,ZIP_CODE,STATE_VOIVODESHIP)
VALUES (6,'ul.Lipowa 55','48','Lodz','91-777','Lodzkie');



--employees
INSERT INTO EMPLOYEE(FIRST_NAME,LAST_NAME,EMPLOYEE_PHONE,EMPLOYEE_EMAIL,ACCOUNT_ID,ADDRESS_ID)
VALUES('Adam','Adamowicz','+48111222333','adamowicz@email.com',1,1);
INSERT INTO EMPLOYEE(FIRST_NAME,LAST_NAME,EMPLOYEE_PHONE,EMPLOYEE_EMAIL,ACCOUNT_ID,ADDRESS_ID)
VALUES('Jan','Kowalski','+48999888777','kowalski@email.com',2,2);
INSERT INTO EMPLOYEE(FIRST_NAME,LAST_NAME,EMPLOYEE_PHONE,EMPLOYEE_EMAIL,ACCOUNT_ID,ADDRESS_ID)
VALUES('Dariusz','Nowak','+48888555111','nowak@email.com',3,3);



--devices
/*INSERT INTO DEVICE(ID,BRAND,MODEL,SERIAL_NUMBER,DESCRIPTION,EXTERNAL_ID)
VALUES(1,'SAMSUNG','ABC','0001234HUAHUA1234','Desc 1','#EXTERNALID9999345');
INSERT INTO DEVICE(ID,BRAND,MODEL,SERIAL_NUMBER,DESCRIPTION,EXTERNAL_ID)
VALUES(2,'LOGITECH','CCC','0007777LLAA5555','Desc 2','#EXTERNALID1114447');
INSERT INTO DEVICE(ID,BRAND,MODEL,SERIAL_NUMBER,DESCRIPTION,EXTERNAL_ID)
VALUES(3,'ASUS','DDDD','7424PINGPONG3787','Desc 3','#EXTERNALID777788*');*/

--customers
INSERT INTO CUSTOMER(FIRST_NAME,LAST_NAME,CUSTOMER_PHONE,CUSTOMER_EMAIL,ACCOUNT_ID,ADDRESS_ID)
VALUES('Adam','Klient','+48123456789','klient1@mail.com',4,4);

INSERT INTO CUSTOMER(FIRST_NAME,LAST_NAME,CUSTOMER_PHONE,CUSTOMER_EMAIL,ACCOUNT_ID,ADDRESS_ID)
VALUES('Bartosz','Klient','+48222333444','klient2@mail.com',5,5);

INSERT INTO CUSTOMER(FIRST_NAME,LAST_NAME,CUSTOMER_PHONE,CUSTOMER_EMAIL,ACCOUNT_ID,ADDRESS_ID)
VALUES('Cezary','Klient','+48147157167','klient3@mail.com',6,6);

