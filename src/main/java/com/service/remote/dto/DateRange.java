package com.service.remote.dto;

import lombok.Value;

import java.time.Instant;

@Value
public class DateRange {
    private Instant from;
    private Instant to;
}