package com.service.remote.service;

import com.service.remote.entity.Customer;
import com.service.remote.entity.security.Account;

import java.util.List;
import java.util.Optional;

public interface CustomerService {
    List<Customer> findAll();
    Optional<Customer> findById(Long id);
    Optional<Customer> findByAccount(Account account);
    void save(Customer customer);
    Customer saveAndReturn(Customer customer);
    void delete(Customer customer);
    void deleteById(Long id);
}
