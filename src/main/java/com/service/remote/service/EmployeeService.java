package com.service.remote.service;

import com.service.remote.entity.Employee;
import com.service.remote.entity.security.Account;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {
    List<Employee> findAll();
    Optional<Employee> findById(Long id);
    Optional<Employee> findByAccount(Account account);
    void save(Employee employee);
    Employee saveAndReturn(Employee employee);
    void delete(Employee employee);
    void deleteById(Long id);
}
