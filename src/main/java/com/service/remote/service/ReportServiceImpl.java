package com.service.remote.service;

import com.service.remote.entity.Customer;
import com.service.remote.entity.Employee;
import com.service.remote.entity.Report;
import com.service.remote.entity.security.Account;
import com.service.remote.repository.ReportRepository;
import com.service.remote.service.security.AccountService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class ReportServiceImpl implements ReportService {
    private final ReportRepository reportRepository;
    private final EmployeeService employeeService;
    private final CustomerService customerService;
    private final AccountService accountService;
    private final static Logger LOG= LoggerFactory.getLogger(ReportServiceImpl.class);
    @Override
    public List<Report> findAll() {
        return reportRepository.findAll();
    }

    @Override
    public Optional<Report> findById(Long id) {
        return reportRepository.findById(id);
    }

    @Override
    public void save(Report report) {
        reportRepository.save(report);
    }

    @Override
    public Report saveAndReturn(Report report) {
        return reportRepository.save(report);
    }

    @Override
    public void delete(Report report) {
        reportRepository.save(report);
    }

    @Override
    public void deleteById(Long id) {
        reportRepository.deleteById(id);
    }
    @Override
    @Transactional
    public void attachReportToCustomer(Report report, String username){
        Optional<Account> accountOptional=accountService.findAccountByUsername(username);
        Optional<Customer> optionalCustomer=customerService.findByAccount(accountOptional.get());
        Customer customer=optionalCustomer.get();
        List<Report> customerReports=customer.getReports();
        if(customerReports == null){
            customerReports=new ArrayList<>();
        }
        customerReports.add(report);
        customer.setReports(customerReports);
        report.setCustomer(customer);
        reportRepository.save(report);
        customerService.save(customer);

    }

    @Override
    @Transactional
    public void attachReportToRandomEmployee(Report report) {
        List<Employee> employees=employeeService.findAll();
        int randomIndex = new Random().nextInt(employees.size());
        LOG.info("randomEmployeeIndex: " + randomIndex);
        Employee employee=employees.get(randomIndex);
        List<Report> employeeReports=employee.getReports();
        if(employeeReports == null){
            employeeReports=new ArrayList<>();
        }
        employeeReports.add(report);
        employee.setReports(employeeReports);
        report.setEmployee(employee);
        reportRepository.save(report);
        employeeService.save(employee);
    }
}
