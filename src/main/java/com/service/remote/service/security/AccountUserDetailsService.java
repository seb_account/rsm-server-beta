package com.service.remote.service.security;

import com.service.remote.entity.security.Account;
import com.service.remote.entity.security.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class AccountUserDetailsService implements UserDetailsService {
    private final AccountService accountService;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Account> accountOptional=accountService.findAccountByUsername(username);
        if(!accountOptional.isPresent()){
            throw new UsernameNotFoundException("Account does not exist");
        }
        Collection<GrantedAuthority> grantedAuthorities=new ArrayList<>();
        Set<Role> roles=accountOptional.map(Account::getRoles).orElse(Collections.EMPTY_SET);
        for(Role role:roles) {
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getRoleName()));
        }

        User userDetails=new User(accountOptional.get().getUsername(),
                accountOptional.get().getPassword(),accountOptional.get().isEnabled(),
                !accountOptional.get().isExpired(),!accountOptional.get().isCredentialsExpired(),
                !accountOptional.get().isLocked(),grantedAuthorities);
        return userDetails;
    }
}
