package com.service.remote.service.security;

import com.service.remote.entity.security.Account;

import java.util.List;
import java.util.Optional;

public interface AccountService {
    List<Account> findAll();
    Optional<Account> findById(Long id);
    Optional<Account> findAccountByUsername(String username);
    void save(Account account);
    Account saveAndReturn(Account account);
    void delete(Account account);
    void deleteById(Long id);
}
