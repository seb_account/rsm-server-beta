package com.service.remote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RsmServerBetaApplication {

	public static void main(String[] args) {
		SpringApplication.run(RsmServerBetaApplication.class, args);
	}
}
