package com.service.remote.repository;

import com.service.remote.entity.Customer;
import com.service.remote.entity.security.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer,Long> {
    Optional<Customer> findByAccount(Account account);
}
