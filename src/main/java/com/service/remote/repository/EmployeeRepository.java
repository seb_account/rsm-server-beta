package com.service.remote.repository;

import com.service.remote.entity.Employee;
import com.service.remote.entity.security.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Long> {
    Optional<Employee> findByAccount(Account account);
}