package com.service.remote.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="DEVICE")
@Getter
@Setter
@EqualsAndHashCode(of={"externalId"})
public class Device extends BaseEntity {
    @Column(name="BRAND")
    private String brand;
    @Column(name="MODEL")
    private String model;
    @Column(name="SERIAL_NUMBER")
    private String serialNumber;
    @Column(name="DESCRIPTION")
    private String description;
    @Column(name="EXTERNAL_ID")
    private String externalId;
}
