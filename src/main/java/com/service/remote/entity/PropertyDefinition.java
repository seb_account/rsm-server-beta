package com.service.remote.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="PROPERTY_DEFINITION")
@Getter
@Setter
public class PropertyDefinition extends BaseEntity {
    @Column(name="CODE")
    private String code;
    @Column(name="NAME")
    private String name;
    @Column(name="MEASURE_UNIT")
    private String measureUnit;
    @Enumerated(EnumType.STRING)
    @Column(name="VALUE_TYPE")
    private ValueType valueType;

}
