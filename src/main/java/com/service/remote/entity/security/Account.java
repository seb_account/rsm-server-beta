package com.service.remote.entity.security;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;


@Entity
@Getter
@Setter
public class Account {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="ACCOUNT_ID")
    private Long accountId;

    @Column(name="USERNAME")
    private String username;

    @Column(name="PASSWORD")
    private String password;

    @Column(name="ENABLED")
    private boolean enabled;

    @Column(name="CREDENTIALS_EXPIRED")
    private boolean credentialsExpired;

    @Column(name="EXPIRED")
    private boolean expired;

    @Column(name="LOCKED")
    private boolean locked;

    @ManyToMany(fetch = FetchType.EAGER,cascade=CascadeType.ALL)
    @JoinTable(name="ACCOUNT_ROLE",joinColumns = @JoinColumn(name="ACCOUNT_ID",
            referencedColumnName = "ACCOUNT_ID"),inverseJoinColumns = @JoinColumn(name="ROLE_ID",
            referencedColumnName = "ROLE_ID"))
    private Set<Role> roles;
}
