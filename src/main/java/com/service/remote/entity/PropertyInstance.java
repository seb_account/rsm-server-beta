package com.service.remote.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="PROPERTY_INSTANCE")
@Getter
@Setter
public class PropertyInstance extends BaseEntity {
    @ManyToOne
    @JoinColumn(name="DEFINITION_ID")
    private PropertyDefinition definition;
    @ManyToOne
    @JoinColumn(name="LOG_DEVICE_PARAMETER_ID")
    @JsonIgnore
    private LogDeviceParameter logDeviceParameter;
    @Column(name="LONG_VALUE")
    private Long longValue;
    @Column(name="STRING_VALUE")
    private String stringValue;
    @Column(name="DOUBLE_VALUE")
    private Double doubleValue;
}