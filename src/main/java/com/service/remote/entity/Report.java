package com.service.remote.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name="REPORT")
@Getter
@Setter
public class Report{
    @Id
    @Column(name="REPORT_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long reportId;
    @Column(name="TITLE")
    private String title;
    @Column(name="DESCRIPTION")
    private String description;

    @JsonIgnore
    @Column(name="REPORT_PHOTO")
    @Lob
    private byte[] reportPhoto;

    @Column(name="REPORT_DATE")
    private LocalDate reportDate;

    @Column(name="REPORT_STATUS")
    @Enumerated(EnumType.STRING)
    private ReportStatus reportStatus;

    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="DEVICE_ID")
    private Device device;

    @ManyToOne
    @JoinColumn(name="EMPLOYEE_ID")
    private Employee employee;

    @ManyToOne
    @JoinColumn(name="CUSTOMER_ID")
    private Customer customer;

}
