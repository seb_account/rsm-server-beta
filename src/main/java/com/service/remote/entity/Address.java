package com.service.remote.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Entity
@Table(name="ADDRESS")
@Getter
@Setter
public class Address {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="ADDRESS_ID")
    private Long addressId;
    @NotEmpty
    @Column(name="STREET_NAME")
    private String streetName;
    @NotEmpty
    @Column(name="APARTMENT_NUMBER")
    private String apartmentNumber;
    @NotEmpty
    @Column(name="CITY")
    private String city;
    @NotEmpty
    @Pattern(regexp = "^\\d{2}-\\d{3}$",message="Please enter a valid zip code")
    @Column(name="ZIP_CODE")
    private String zipCode;
    @NotEmpty
    @Column(name="STATE_VOIVODESHIP")
    private String stateVoivodeship;
}
