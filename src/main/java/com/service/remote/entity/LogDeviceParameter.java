package com.service.remote.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Entity
@Table(name="LOG_DEVICE_PARAMETER")
@Getter
@Setter
public class LogDeviceParameter extends BaseEntity {
    @Column(name="LOG_DATE")
    private Instant logDate;
    @ManyToOne
    @JoinColumn(name="DEVICE_ID")
    private Device device;

    @OneToMany(mappedBy = "logDeviceParameter")
    private List<PropertyInstance> properties;
}
