package com.service.remote.entity;

public enum ValueType {
    LONG,
    DOUBLE,
    STRING
}